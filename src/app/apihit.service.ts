import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from '../../node_modules/rxjs';
import { Userdetails } from './userdetails';


@Injectable({
  providedIn: 'root'
})
export class ApihitService {
  user = new Userdetails();
  constructor(private http : HttpClient) { 
    this.user = JSON.parse(localStorage.getItem("key"));
  }
  

  httpOptions = {
    headers : new HttpHeaders ({
      'Content-Type' : 'application/x-www-form-urlencoded',
      'Authorization' : 'Basic QUM1ZjExM2U5MDMyYTk1NDhkYWZiOTMwOWVjMWU5NDZkNzphNWNjNTVkZDlmOWI0ZTQ1ZDdkNThhN2ZjZDEyMDAyYg=='
    })
  };
  url = 'https://chat.twilio.com/v2/Services';
  Service = 'https://chat.twilio.com/v2/Services/IS6fc5291ab2ed4d1c8327dff1f2c0408b/';
  Servicesid = 'ServiceSid=IS6fc5291ab2ed4d1c8327dff1f2c0408b';
  SetData():Observable<any> {
    return this.http.post(this.url,'FriendlyName=Rahul', this.httpOptions);
  }
  CreateChannel():Observable<any> {
    return this.http.post(this.Service+'Channels', 'UniqueName=General', this.httpOptions);
  }
  DisplayAllChannel():Observable<any> {
    return this.http.get(this.Service+'Channels', this.httpOptions);
  }
  AddChannel(str):Observable<any> {
    return this.http.post(this.Service+'Channels', 'UniqueName='+str, this.httpOptions);
  } 
  DeleteChannel(str):Observable<any> {
    return this.http.delete(this.Service+'Channels/'+str, this.httpOptions);
  } 
  RetrieveChannelId(str):Observable<any> {
    return this.http.get(this.Service+'Channels/'+str, this.httpOptions);
  }
  RetrieveChannelName(str):Observable<any> {
    return this.http.get(this.Service+'Channels/'+str, this.httpOptions);
  }
  ChannelAddMember(str):Observable<any> {
    return this.http.post(this.Service+'Channels/'+str+'/Members', 'ChannelSid='+str+'&Identity='+this.user.email+'&'+this.Servicesid, this.httpOptions);
  }
  AddRole():Observable<any> {
    return this.http.post(this.Service+'Roles', 'FriendlyName=Rahul&Permission=createChannel&Type=deployment', this.httpOptions);
  }
  ListAllUsers():Observable<any> {
    return this.http.get(this.Service+'Users', this.httpOptions);
  }
  RetrieveUser():Observable<any> {
    return this.http.get(this.Service+'Users/'+this.user.email, this.httpOptions);
  }
  IsSubscribed(str) :Observable<any> {
    return this.http.get(this.Service+'Users/'+str+'/Channels', this.httpOptions);
  }
  SendMessage(message, channel):Observable<any> {
    return this.http.post(this.Service+'Channels/'+channel+'/Messages',  'ChannelSid='+channel+'&'+this.Servicesid+'&From='+this.user.email+'&Body='+message, this.httpOptions);
  }
  ShowAllMessages(str):Observable<any> {
    return this.http.get(this.Service+'Channels/'+str+'/Messages', this.httpOptions);
  }
  DetailUser():Observable<any> {
    return this.http.get(this.Service+'Users/'+this.user.email, this.httpOptions);
  }
  MembersOfChannel(str):Observable<any> {
    return this.http.get(this.Service+'Channels/'+str+'/Members/', this.httpOptions);
  }
}
