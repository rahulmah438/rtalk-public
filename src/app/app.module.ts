import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ChatComponent } from './chat/chat.component';
import { ColorDirective } from './color.directive';
import { HttpClientModule } from '@angular/common/http';
import { AuthorizationService } from './authorization.service';
import { ApihitService } from './apihit.service';
import { FormsModule } from '@angular/forms'
import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from "angular-6-social-login";
import { ColorWhiteDirective } from './color-white.directive';
import { WhitePlusDirective } from './white-plus.directive';
import { AuthorizationChatService } from './authorization-chat.service';

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider("874636830594-putgitnu65ibeocd6r956d9ak8e46puv.apps.googleusercontent.com")
        },
      ]);
    return config;
  }
  const route:Routes=[

    {
      path:   '',
      component:LoginComponent
      
    },
  {
    path:   'login',
    component:LoginComponent,
    canActivate: [AuthorizationChatService]
  },
  {
    path:   'chatapp',
    component:ChatComponent,
    canActivate: [AuthorizationService]
  }
  
  ];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ChatComponent,
    ColorDirective,
    ColorWhiteDirective,
    WhitePlusDirective
  ],
  imports: [
    BrowserModule,
    SocialLoginModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(route)
  ],
  providers: [ApihitService, AuthorizationService, AuthorizationChatService,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
