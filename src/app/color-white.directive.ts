import { Directive, ElementRef, Renderer } from '@angular/core';

@Directive({
  selector: '[appColorWhite]'
})
export class ColorWhiteDirective {

  constructor(private ref : ElementRef, private renderer : Renderer) {
    this.changeColor('white')
   }
   changeColor(color : any) {
     this.ref.nativeElement.style.color = color;
   }
}
