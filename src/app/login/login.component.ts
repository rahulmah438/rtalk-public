import { Component, OnInit } from '@angular/core';
import { AuthorizationService} from '../authorization.service';
import { FormsModule } from '@angular/forms';
import {
  AuthService,
  GoogleLoginProvider, 
} from 'angular-6-social-login';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private socialAuthService: AuthService, private route: Router) { }

  ngOnInit() {
  }
  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID; 
    if  (localStorage.getItem("Token") == "Rahul") {
      this.route.navigate(['/chatapp']);
    }
    else {
    this.socialAuthService.signIn(socialPlatformProvider).then(
    
      (userData) => {
        localStorage.setItem('key',JSON.stringify(userData));
        localStorage.setItem('Token', 'Rahul');
        localStorage.setItem("Channel", "General");
        this.route.navigate(['/chatapp']);
      }
    );
  }
}
}
