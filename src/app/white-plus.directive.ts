import { Directive, ElementRef, HostListener, Renderer } from '@angular/core';

@Directive({
  selector: '[appWhitePlus]'
})
export class WhitePlusDirective {

  constructor(private ref : ElementRef, private renderer : Renderer) {
    this.changeColor('#cac4c9')
   }
   @HostListener('mouseover') onMouseOver() {
    this.changeColor('white');
  }
  @HostListener('mouseleave') onMouseLeave() {
    this.changeColor('#cac4c9');
  }
  changeColor(color : any) {
    this.ref.nativeElement.style.color = color;
  }

}
