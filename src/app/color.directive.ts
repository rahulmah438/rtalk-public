import { Directive, ElementRef, HostListener, Renderer } from '@angular/core';

@Directive({
  selector: '[appColor]'
})
export class ColorDirective {

  constructor(private ref : ElementRef, private renderer : Renderer) {
    this.changeColor('#cac4c9')
   }
   @HostListener('mouseover') onMouseOver() {
    this.ChangeBgColor('#D2691E');
  }
  // @HostListener('click') onClick() {
  //   this.highlight('#4D9689');
  // }
  @HostListener('mouseleave') onMouseLeave() {
    this.ChangeBgColor('#cac4c9');
  }
   changeColor(color : any) {
     this.ref.nativeElement.style.color = color;
   }
   ChangeBgColor(color: string) {
    this.renderer.setElementStyle(this.ref.nativeElement, 'color', color);
}
highlight(color) {
  this.renderer.setElementStyle(this.ref.nativeElement, 'backgroundColor', color);
}
}
