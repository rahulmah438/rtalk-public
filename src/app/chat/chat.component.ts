import { Component, OnInit, OnDestroy } from '@angular/core';
import { 
  AuthService,
  GoogleLoginProvider, 
} from 'angular-6-social-login';
import { Router } from '@angular/router';
import { Response } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Alert } from '../../../node_modules/@types/selenium-webdriver';
import { Userdetails } from '../userdetails';
import { ApihitService } from '../apihit.service';
import { FormsModule } from '@angular/forms'

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  user = new Userdetails();
  Channel : string;
  choice : string;
  length : number;
  txt : string;
  ChannelArr = [];
  ChannelId = [];
  ChannelName = [];
  UserSid : string;
  msgArr : Array<{body: any, from : string, frombracket : string, time: string}> = [];
  msg : string;
  SetInt : any;
  SetInt2 : any;
  SearchedChannel : string;
  regValue : any;
  SearchedChannelArr = [];
  count : number;
  IsClicked1 : Boolean = true;
  IsClicked2 : Boolean = true;
  constructor(private socialAuthService: AuthService, private route : Router, 
    private apihit : ApihitService) { 
    this.Channel = localStorage.getItem("Channel");
    this.user = JSON.parse(localStorage.getItem("key"));

    this.SetInt = setInterval(() => {
      this.apihit.ShowAllMessages(this.Channel).subscribe(res => 
       { 
         this.msgArr.length =0;
         this.length = res.messages.length;
         for ( let i = 0; i < this.length; i++){ 
           
           this.msgArr.push({
             body : res.messages[i].body,
             frombracket : '(' + res.messages[i].from + ')',
             from : res.messages[i].from,
             time : res.messages[i].date_created
         })
        }}),
       err => {
         console.log(err);
       }
     },3000)
  //    this.SetInt2 = setInterval(() => {
  // this.apihit.RetrieveUser().subscribe(res => 
  //   { 
  //   console.log(res);
  //   this.apihit.IsSubscribed(res.sid).subscribe(res => 
  //     { 
  //       this.length = res.channels.length;
  //       for ( let i = 0; i < this.length; i++){ 
            
  //         this.ChannelId[i] = res.channels[i].channel_sid;
  //         console.log(this.ChannelId);
  //       }
  //       for ( let i = 0; i < this.length; i++){ 
            
  //         this.apihit.RetrieveChannelName(this.ChannelId[i]).subscribe(res => 
  //           { 
  //             // this.ChannelName=res;
  //             this.ChannelName[i]=res.unique_name;
  //             console.log(this.ChannelName[i]);
  //         }),
  //           err => {
  //             console.log(err);
  //         } }
  //     }),
  //     err => {
  //       console.log(err);
  //     }
  //   }),
  //       err => {
  //         console.log(err);
  //       }
  //   }
  //    ), 5000
}
      

ngOnInit() {
this.apihit.RetrieveChannelId(this.Channel).subscribe(res => 
  { 
  this.apihit.MembersOfChannel(res.sid).subscribe(res => 
    { 
      console.log(res);
      this.count = res.members.length;
      }),
    err => {
      console.log(err);
    }
    });
    err => {
      console.log(err);
    }
  
}
  ngOnDestroy() {
    clearInterval(this.SetInt);
    clearInterval(this.SetInt2);
  }

Notification() {
  alert("You have no notifications right now");
}
AlertBoxUnread(){
  alert("You have no unread messages right now");
}
AlertBoxThread(){
  alert("You have no threads right now");
}
Information(){
  alert("You have no specific information right now");
}
Settings(){
  alert("You have no active settings right now");
}
Phone(){
  alert("You have no phone calls right now");
}

AddChannelIconClicked() {
this.txt = prompt("Please enter channel name", "");
if (this.txt == null || this.txt == "") {
alert("you did not enter any channel name");
return;
}
else {
this.choice = this.txt;
this.apihit.AddChannel(this.choice).subscribe(res => 
        { 
       alert("Channel successfully created");
       this.apihit.DisplayAllChannel().subscribe(res => 
        { 
          this.length = res.channels.length;
          this.ChannelArr.length = this.length;
          for ( let i = 0; i < this.length; i++){
              this.ChannelArr[i] = res.channels[i].unique_name;
          }
            }),
            err => {
              console.log(err);
            }
        }),
        err => {
          console.log(err);
        }
  } 
}

RemoveChannelIconClicked() {
this.txt = prompt("Please enter channel name", "");
if (this.txt == null || this.txt == "") {
alert("you did not enter any channel name");
return;
}
else {
this.choice = this.txt;
this.apihit.DeleteChannel(this.choice).subscribe(res => 
        { 
          alert("Channel successfully deleted");
          this.apihit.DisplayAllChannel().subscribe(res => 
            { 
              this.length = res.channels.length;
              this.ChannelArr.length = this.length;
              for ( let i = 0; i < this.length; i++){
                  this.ChannelArr[i] = res.channels[i].unique_name;
              }
                }),
                err => {
                  console.log(err);
                }
        }),
        err => {
          console.log(err);
        }
  } }
DisplayAllChannels(){
this.apihit.DisplayAllChannel().subscribe(res => 
  { 
    console.log(res);
    this.length = res.channels.length;
    for ( let i = 0; i < this.length; i++){
        this.ChannelArr[i] = res.channels[i].unique_name;
    }
    this.IsClicked2 = false;
      }),
  err => {
    console.log(err);
  }
}
SubscribedChannels(){
  console.log("In");
this.apihit.RetrieveUser().subscribe(res => 
{ 
console.log(res);
this.apihit.IsSubscribed(res.sid).subscribe(res => 
  { 
    this.length = res.channels.length;
    for ( let i = 0; i < this.length; i++){ 
        
      this.ChannelId[i] = res.channels[i].channel_sid;
      console.log(this.ChannelId);
    }
    this.ChannelName.length = 0;
    for ( let i = 0; i < this.length; i++){ 
        
      this.apihit.RetrieveChannelName(this.ChannelId[i]).subscribe(res => 
        { 
          // this.ChannelName=res;
          this.ChannelName[i]=res.unique_name;
      }),
        err => {
          console.log(err);
      } }
      this.IsClicked1 = false;
  }),
  err => {
    console.log(err);
  }
}),
    err => {
      console.log(err);
    }
}
Logout(){
  this.socialAuthService.signOut().then(
  (userData) => {
  localStorage.clear();
  this.route.navigate(['/login']);
    }
  );
}

CreateServiceFunction (){
  this.apihit.SetData().subscribe(res => 
  { 
    console.log(res); 
  }),
  err => {
    console.log(err);
  }
}

CreateChannelFunction (){
  this.apihit.CreateChannel().subscribe(res => 
        { 
          console.log(res); 
        }),
        err => {
          console.log(err);
        }
      } 
Send(str) {
this.apihit.SendMessage(str, this.Channel).subscribe(res => 
      { 
        this.msg = '';
        this.apihit.ShowAllMessages(this.Channel).subscribe(res => 
          { 
            this.msgArr.length =0;
            this.length = res.messages.length;
            console.log(res);
            for ( let i = 0; i < this.length; i++){ 
              
              this.msgArr.push({
                body : res.messages[i].body,
                frombracket : '(' + res.messages[i].from + ')',
                from : res.messages[i].from,
                time : res.messages[i].date_created
            })
            }}),
          err => {
            console.log(err);
          }
      }),
      err => {
        console.log(err);
      }
}  
// History() {
    //       this.AuthObject.ShowAllMessages().subscribe(res => 
    //                 { 
    //                   this.length = res.messages.length;
    //                   console.log(res);
    //                   for ( let i = 0; i < this.length; i++){ 
                        
    //                   this.msgArr[i] = res.messages[i].body +'(' + res.messages[i].from + ')'; 
    //                   // this.msgArr.reverse;
    //               }
    //                 }),
    //                 err => {
    //                   console.log(err);
    //                 }
    //          }  
    
SearchChannel() {
this.apihit.DisplayAllChannel().subscribe(res => 
  { 
    console.log(res);
    this.length = res.channels.length;
    for ( let i = 0; i < this.length; i++){
        this.ChannelArr[i] = res.channels[i].unique_name;
    }
    if (this.SearchedChannel.length < 1) {
      return;
    }
    this.SearchedChannel = this.SearchedChannel.toLowerCase();
    this.regValue = new RegExp(this.SearchedChannel, "i");
    this.SearchedChannelArr.length = 0;
    for (let SearchResult of this.ChannelArr) {
      if (this.regValue.test(SearchResult)) {
        this.SearchedChannelArr.push(SearchResult)
      }
    }
      }),
  err => {
    console.log(err);
  }
}

AllChannel(str) {
  this.apihit.RetrieveChannelId(str).subscribe(res => 
   { 
    this.apihit.ChannelAddMember(res.sid).subscribe(res => 
      { 
        alert("You joined to this channel successfully");
        this.SubscribedChannels();
      }),
      err => {
        alert("You are already member of this channel");
      }
     });
     err => {
       console.log(err);
     }
}

ViewChannelMessages(str) {
  this.Channel = str;
  localStorage.setItem("Channel", this.Channel)
  this.apihit.RetrieveChannelId(str).subscribe(res => 
   { 
    this.apihit.ShowAllMessages(res.sid).subscribe(res => 
      { 
        this.msgArr.length =0;
        this.length = res.messages.length;
        console.log(res);
        for ( let i = 0; i < this.length; i++){ 
          
          this.msgArr.push({
            body : res.messages[i].body,
            frombracket : '(' + res.messages[i].from + ')',
            from : res.messages[i].from,
            time : res.messages[i].date_created
        })
       }}),
      err => {
        console.log(err);
      }
     });
     err => {
       console.log(err);
     }
   
 }
 
}

