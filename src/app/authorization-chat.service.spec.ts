import { TestBed, inject } from '@angular/core/testing';

import { AuthorizationChatService } from './authorization-chat.service';

describe('AuthorizationChatService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthorizationChatService]
    });
  });

  it('should be created', inject([AuthorizationChatService], (service: AuthorizationChatService) => {
    expect(service).toBeTruthy();
  }));
});
