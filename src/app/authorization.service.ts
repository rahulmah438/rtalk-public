import { Injectable } from '@angular/core';
import { CanActivate, Router } from '../../node_modules/@angular/router';
import { RouterModule} from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthorizationService implements CanActivate {

  constructor(private route : Router) { }
  canActivate() {
    if(localStorage.getItem('Token')==='Rahul') {
    return true;
  }
  else {
    this.route.navigate(['/login']);
    return false;
  }
}
}
