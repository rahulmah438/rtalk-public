import { Injectable } from '@angular/core';
import { CanActivate } from '../../node_modules/@angular/router';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationChatService implements CanActivate{

  constructor (private route : Router) {}
  canActivate() {
    if (localStorage.getItem("Token") == "Rahul") {
      alert('You are signed in.');
      this.route.navigate(['/chatapp']);
      return false;
    }
    return true;
  }
}
